## To Do: 
- Add ID field
- Refactor (Including: Clean Architecture (Split Repository into Query and Repository) and Clean Code)

## Doing:
- Cockroach Repository (Save, Find Published Article, Find Article By Slug, and Find Article By Category)
- CLI
- Testing

## Done:
- Domain
- In Memory Repository (Save, Find Published Article, Find Article By Slug, and Find Article By Category)
- SQLite Repository (Save, Find Published Article, Find Article By Slug, and Find Article By Category)