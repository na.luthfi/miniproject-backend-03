package domain

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

type Article struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Status    Status
	Category  []Category
}

type Category string
type Status string

const (
	Politic       Category = "Politic"
	News          Category = "News"
	Internet      Category = "Internet"
	Entertainment Category = "Entertainment"
)

const (
	ArticlePublished    Status = "Published"
	ArticleNotPublished Status = "NotPublished"
)

type PostService interface {
	CheckSlugIsExist(slug string) bool
}

func CreateArticle(ps PostService, slug, name, body string, status Status, categories ...Category) (Article, error) {
	//WHEN
	//Generate slug if slug value is empty. If slug value is not empty, check slug is exist or not.
	slugExist := ps.CheckSlugIsExist(slug)
	fmt.Printf("isi slug: %s\n", slug)
	if len(slug) == 0 {
		temp := strings.Fields(name)
		slug = strings.Join(temp, "-")
		timeStamp := time.Now().UnixNano()
		timeString := fmt.Sprintf("%d", timeStamp)
		slug += "-" + timeString
	} else {
		if slugExist {
			return Article{}, errors.New("Change slug, slug is already used")
		}
	}

	//name must contain value
	if name == "\n" {
		return Article{}, fmt.Errorf("Name can't be empty")
	}

	//body must contain value
	if body == "\n" {
		return Article{}, fmt.Errorf("Body can't be empty")
	}

	//categories must contain value
	if len(categories) == 0 {
		return Article{}, fmt.Errorf("Category needed")
	}

	for _, category := range categories {
		switch category {
		case Politic, News, Internet, Entertainment:
		default:
			return Article{}, fmt.Errorf("Category not valid")
		}
	}

	//THEN
	return Article{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
		Status:    status,
		Category:  categories,
	}, nil
}
