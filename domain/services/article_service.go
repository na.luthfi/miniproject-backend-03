package services

import (
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
)

type ArticleServiceInMemory struct {
	ArticleRepository repository.ArticleRepository
}

func NewArticleServiceInMemory(repo repository.ArticleRepository) *ArticleServiceInMemory {
	return &ArticleServiceInMemory{
		ArticleRepository: repo,
	}
}

func (a *ArticleServiceInMemory) CheckSlugIsExist(slug string) bool {
	exist := a.ArticleRepository.IsSlugExist(slug)
	return exist
}
