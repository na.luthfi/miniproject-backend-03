package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type MockArticleServiceSuccess struct {
}

func (as MockArticleServiceSuccess) CheckSlugIsExist(name string) bool {
	return false
}

type MockArticleServiceError struct {
}

func (as MockArticleServiceError) CheckSlugIsExist(name string) bool {
	return true
}
func TestCanCreateArticle(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "Permintaan Westlife saat Konset di Indonesia"
	slug := "westlife"
	body := "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura."
	var status Status = ArticlePublished
	var categories = []Category{Entertainment, News}

	// When
	got, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Equal(t, categories, got.Category)
	assert.NoError(t, err)
}

func TestCantCreateArticleIfCategoryInvalid(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "7 Pilihan Topik Obrolan dengan Anak Balita"
	slug := "topik-balita"
	body := "Balita Anda semakin hari sudah semakin pintar bicaranya ya, Moms. Kalau Anda terus merangsangnya dengan mengajaknya bicara, ia pun juga semakin terampil dan kaya kosakatanya, lho. Berbeda dengan bayi, balita sudah bisa menjawab perkataan Anda, meski kalimatnya pendek-pendek dan tak lengkap. Diskusi dengan si kecil pun tak lagi hanya sepihak."
	var status Status = ArticlePublished
	categories := []Category{"Mom"}

	// When
	_, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category not valid", err.Error())
}

func TestCantCreateArticleIfNameIsBlank(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "\n"
	slug := "westlife"
	body := "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura."
	var status Status = ArticlePublished
	var categories = []Category{Entertainment, News}

	// When
	_, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Name can't be empty", err.Error())
}

func TestCantCreateArticleIfBodyIsBlank(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "Permintaan Westlife saat Konser di Indonesia"
	slug := "westlife"
	body := "\n"
	var status Status = ArticlePublished
	var categories = []Category{Entertainment, News}

	// When
	_, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Body can't be empty", err.Error())
}

func TestCantCreateArticleIfCategoryIsBlank(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "Permintaan Westlife saat Konser di Indonesia"
	slug := "westlife"
	body := "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura."
	var status Status = ArticlePublished
	var categories = []Category{}

	// When
	_, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category needed", err.Error())
}

func TestCanCreateArticleWithBlankSlug(t *testing.T) {
	// Given
	as := MockArticleServiceSuccess{}
	name := "Permintaan Westlife saat Konser di Indonesia"
	slug := ""
	body := "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura."
	var status Status = ArticlePublished
	var categories = []Category{Entertainment}

	// When
	got, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.Empty(t, err)
	assert.NotEmpty(t, got.Slug)
}

func TestCantCreateArticleIfSlugIsExist(t *testing.T) {
	// Given
	as := MockArticleServiceError{}
	name := "Permintaan Westlife saat Konser di Indonesia"
	slug := "westlife"
	body := "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura."
	var status Status = ArticlePublished
	var categories = []Category{Entertainment}

	// When
	_, err := CreateArticle(as, slug, name, body, status, categories...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Change slug, slug is already used", err.Error())
}
