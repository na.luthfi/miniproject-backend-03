package storage

import "gitlab.com/na.luthfi/miniproject-backend-03/domain"

// ArticleStorage is storage in memory
type ArticleStorage struct {
	ArticleMap map[string]domain.Article
}

// CreateArticleStorage is to create article storage instance
func CreateArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: make(map[string]domain.Article),
	}
}

// PageStorage is storage in memory
type PageStorage struct {
	PageMap map[string]domain.Page
}

// CreatePageStorage is to create article storage instance
func CreatePageStorage() *PageStorage {
	return &PageStorage{
		PageMap: make(map[string]domain.Page),
	}
}
