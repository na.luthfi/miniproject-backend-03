module gitlab.com/na.luthfi/miniproject-backend-03

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	gitlab.com/fannyhasbi/article-service v0.0.0-20190720144541-ee6b3dc60823
	gitlab.com/na.luthfi/miniproject-backend-03.git v0.0.0-20190721100559-2372d5462570
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
)
