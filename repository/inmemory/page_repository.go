package inmemory

import (
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
	"gitlab.com/na.luthfi/miniproject-backend-03/storage"
)

const PageLimit = 3

type InMemoryPageStorage struct {
	Storage *storage.PageStorage
}

func NewPageRepositoryInMemory(storage *storage.PageStorage) repository.PageRepository {
	return &InMemoryPageStorage{
		Storage: storage,
	}
}

// SavePage create a key and value into StorageMap
func (ps *InMemoryPageStorage) SavePage(page *domain.Page) error {
	ps.Storage.PageMap[page.Slug] = *page
	return nil
}

// IsPageLimited check whether the page is more than the limit
func (ps *InMemoryPageStorage) IsPageLimited() bool {
	if len(ps.Storage.PageMap) >= PageLimit {
		return true
	}

	return false
}

func (ps *InMemoryPageStorage) GetAllPages() repository.QueryResult {
	result := repository.QueryResult{}
	var pages []domain.Page
	for _, page := range ps.Storage.PageMap {
		pages = append(pages, page)
	}

	result.Result = pages

	return result
}

func (ps *InMemoryPageStorage) IsSlugExist(slug string) bool {
	for _, post := range ps.Storage.PageMap {
		if post.Slug == slug {
			return true
		}
	}
	return false
}

type PageStorageSuccess struct{}

func (ps PageStorageSuccess) IsPageLimited() bool {
	return false
}

func (ps PageStorageSuccess) CheckSlugIsExist(slug string) bool {
	return false
}

func (ps PageStorageSuccess) GetAllPages() []domain.Page {
	return []domain.Page{}
}
