package inmemory

import (
	"errors"

	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
	"gitlab.com/na.luthfi/miniproject-backend-03/storage"
)

type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{
		Storage: storage,
	}
}

func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) error {
	repo.Storage.ArticleMap[article.Slug] = *article

	return nil
}

func (repo *ArticleRepositoryInMemory) IsSlugExist(slug string) bool {
	for _, article := range repo.Storage.ArticleMap {
		if article.Slug == slug {
			return true
		}
	}
	return false
}

func (repo *ArticleRepositoryInMemory) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}
	article, ok := repo.Storage.ArticleMap[slug]
	if !ok {
		result.Result = nil
		result.Error = errors.New("Slug doesn't exist")
	} else {
		result.Result = article
		result.Error = nil
	}

	return result
}

func (repo *ArticleRepositoryInMemory) ListPublishedArticle() repository.QueryResult {
	result := repository.QueryResult{}
	var articles []domain.Article
	for _, article := range repo.Storage.ArticleMap {
		if article.Status == domain.ArticlePublished {
			articles = append(articles, article)
		}
	}

	result.Result = articles

	return result
}

func (repo *ArticleRepositoryInMemory) ListPublishedArticleByCategory(category string) repository.QueryResult {
	result := repository.QueryResult{}
	var articles []domain.Article
	for _, article := range repo.Storage.ArticleMap {
		if article.Status == domain.ArticlePublished {
			for _, categoryResult := range article.Category {
				if categoryResult == domain.Category(category) {
					articles = append(articles, article)
				}
			}
		}
	}

	result.Result = articles

	return result
}
