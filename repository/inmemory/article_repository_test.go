package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/storage"
)

func TestCanFindPublishedArticle(t *testing.T) {
	// given
	article1 := domain.Article{
		Name:     "Permintaan Westlife saat Konser di Indonesia",
		Slug:     "westlife",
		Body:     "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.Entertainment},
	}

	article2 := domain.Article{
		Name:     "7 Pilihan Topik Obrolan dengan Anak Balita",
		Slug:     "topik-balita",
		Body:     "Balita Anda semakin hari sudah semakin pintar bicaranya ya, Moms. Kalau Anda terus merangsangnya dengan mengajaknya bicara, ia pun juga semakin terampil dan kaya kosakatanya, lho. Berbeda dengan bayi, balita sudah bisa menjawab perkataan Anda, meski kalimatnya pendek-pendek dan tak lengkap. Diskusi dengan si kecil pun tak lagi hanya sepihak.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.News},
	}

	articles := []domain.Article{article1, article2}
	storage := storage.ArticleStorage{
		ArticleMap: map[string]domain.Article{
			article1.Slug: article1,
			article2.Slug: article2,
		},
	}
	repo := NewArticleRepositoryInMemory(&storage)

	// when
	result := repo.ListPublishedArticle()

	// then
	assert.Equal(t, articles, result.Result)
}

func TestCanFindBySlug(t *testing.T) {
	// given
	article1 := domain.Article{
		Name:     "Permintaan Westlife saat Konser di Indonesia",
		Slug:     "westlife",
		Body:     "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.Entertainment},
	}

	article2 := domain.Article{
		Name:     "7 Pilihan Topik Obrolan dengan Anak Balita",
		Slug:     "topik-balita",
		Body:     "Balita Anda semakin hari sudah semakin pintar bicaranya ya, Moms. Kalau Anda terus merangsangnya dengan mengajaknya bicara, ia pun juga semakin terampil dan kaya kosakatanya, lho. Berbeda dengan bayi, balita sudah bisa menjawab perkataan Anda, meski kalimatnya pendek-pendek dan tak lengkap. Diskusi dengan si kecil pun tak lagi hanya sepihak.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.News},
	}

	storage := storage.ArticleStorage{
		ArticleMap: map[string]domain.Article{
			article1.Slug: article1,
			article2.Slug: article2,
		},
	}
	repo := NewArticleRepositoryInMemory(&storage)

	// when
	result := repo.FindBySlug("westlife")

	// then
	assert.Equal(t, article1.Slug, result.Result.(domain.Article).Slug)
}

func TestCanFindPublishedArticleByCategory(t *testing.T) {
	// given
	article1 := domain.Article{
		Name:     "Permintaan Westlife saat Konser di Indonesia",
		Slug:     "westlife",
		Body:     "Westlife akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.Entertainment},
	}

	article2 := domain.Article{
		Name:     "7 Pilihan Topik Obrolan dengan Anak Balita",
		Slug:     "topik-balita",
		Body:     "Balita Anda semakin hari sudah semakin pintar bicaranya ya, Moms. Kalau Anda terus merangsangnya dengan mengajaknya bicara, ia pun juga semakin terampil dan kaya kosakatanya, lho. Berbeda dengan bayi, balita sudah bisa menjawab perkataan Anda, meski kalimatnya pendek-pendek dan tak lengkap. Diskusi dengan si kecil pun tak lagi hanya sepihak.",
		Status:   domain.ArticlePublished,
		Category: []domain.Category{domain.News},
	}

	article3 := domain.Article{
		Name:     "Permintaan Jason saat Konser di Indonesia",
		Slug:     "jason",
		Body:     "Jason akan menggelar konser pada tanggal 6-7 Agustus di ICE BSD, Tangerang Selatan.  Dalam konser yang bertajuk 'Westlife - The Twenty Tour 2019,'  grup vokal asal Irlandia ini akan membawakan lagu-lagu populernya dan bernostalgia bersama penggemarnya mengenai perjalanan kembali dari tahun 1999 sampai di 2019. Karena akan menjadi konser yang mewah dan megah, Westlife menyampaikan permintaan khusus kepada promotor untuk disediakan beberapa fasilitas. Diantaranya adalah disediakannya permen khusus tenggorokan Volcazone yang akan diimpor dari Singapura.",
		Status:   domain.ArticleNotPublished,
		Category: []domain.Category{domain.Entertainment},
	}

	articles := []domain.Article{article1}
	storage := storage.ArticleStorage{
		ArticleMap: map[string]domain.Article{
			article1.Slug: article1,
			article2.Slug: article2,
			article3.Slug: article3,
		},
	}
	repo := NewArticleRepositoryInMemory(&storage)

	// when
	result := repo.ListPublishedArticleByCategory("Entertainment")

	// then
	assert.Equal(t, articles, result.Result)
}
