package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/storage"
)

func TestSlugIsExist(t *testing.T) {
	// given
	page1 := domain.Page{
		Name:   "FAQ",
		Slug:   "faq",
		Body:   "Bagian ini berisi pertanyaan yang sering muncul",
		Status: domain.PagePublished,
	}

	page2 := domain.Page{
		Name:   "Help",
		Slug:   "help",
		Body:   "Bagian ini berisi penjelasan penggunaan untuk pengguna",
		Status: domain.PagePublished,
	}

	storage := storage.PageStorage{
		PageMap: map[string]domain.Page{
			page1.Slug: page1,
			page2.Slug: page2,
		},
	}
	repo := NewPageRepositoryInMemory(&storage)

	// when
	result := repo.IsSlugExist("faq")

	// then
	assert.Equal(t, true, result)
}

// func TestCanSavePageIntoStorage(t *testing.T) {
// 	// given
// 	pageService := PageServiceSuccess{}
// 	page1, _ := domain.CreatePage(pageService, "About Us", "about-us", "This is about us page")
// 	page2, _ := domain.CreatePage(pageService, "FAQ", "faq", "Frequently Asked Questions")

// 	// when
// 	storage.SavePage(&page1)
// 	storage.SavePage(&page2)

// 	// then
// 	assert.Equal(t, storage.PageMap, map[string]domain.Page{
// 		page1.Slug: page1,
// 		page2.Slug: page2,
// 	})
// }

func TestCanGetAllPageFromStorage(t *testing.T) {
	// given
	page1 := domain.Page{
		Name:   "FAQ",
		Slug:   "faq",
		Body:   "Bagian ini berisi pertanyaan yang sering muncul",
		Status: domain.PagePublished,
	}

	page2 := domain.Page{
		Name:   "Help",
		Slug:   "help",
		Body:   "Bagian ini berisi penjelasan penggunaan untuk pengguna",
		Status: domain.PagePublished,
	}

	storage := storage.PageStorage{
		PageMap: map[string]domain.Page{
			page1.Slug: page1,
			page2.Slug: page2,
		},
	}
	repo := NewPageRepositoryInMemory(&storage)

	pages := []domain.Page{page1, page2}
	// when
	result := repo.GetAllPages()

	// then
	assert.Equal(t, pages, result.Result)
}
