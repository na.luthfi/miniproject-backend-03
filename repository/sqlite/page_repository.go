package sqlite

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
)

const PageLimit = 3

type PageRepositorySQLite struct {
	DB *sql.DB
}

func NewPageRepositorySQLite(DB *sql.DB) repository.PageRepository {
	return &PageRepositorySQLite{DB: DB}
}

func (repo *PageRepositorySQLite) IsPageLimited() bool {
	result := repository.QueryResult{}
	pages := []domain.Page{}
	rows, err := repo.DB.Query(`SELECT * FROM PAGES`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			page := domain.Page{}
			rows.Scan(
				&page.Slug,
				&page.Name,
				&page.Body,
				&page.CreatedAt,
				&page.Status,
			)

			pages = append(pages, page)
		}

		result.Result = pages
	}

	if len(pages) >= PageLimit {
		return true
	}

	return false
}

func (repo *PageRepositorySQLite) IsSlugExist(slug string) bool {
	result := repository.QueryResult{}
	page := domain.Page{}

	row := repo.DB.QueryRow(`SELECT * FROM PAGES WHERE SLUG = ?`, slug)

	row.Scan(
		&page.Slug,
		&page.Name,
		&page.Body,
		&page.Status,
	)

	result.Result = page

	if page.Slug == slug {
		return true
	}

	return false
}

func (repo *PageRepositorySQLite) SavePage(page *domain.Page) error {
	statement, err := repo.DB.Prepare(`INSERT INTO PAGES(SLUG, NAME, BODY, CREATEDAT, STATUS) VALUES(?,?,?,?,?)`)
	if err != nil {
		return err
	}

	statement.Exec(page.Slug, page.Name, page.Body, page.CreatedAt, page.Status)

	return nil
}

func (repo *PageRepositorySQLite) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}
	page := domain.Page{}

	row := repo.DB.QueryRow(`SELECT * FROM PAGES WHERE SLUG = ?`, slug)

	row.Scan(
		&page.Slug,
		&page.Name,
		&page.Body,
		&page.Status,
	)

	result.Result = page

	return result
}

func (repo *PageRepositorySQLite) ListPublishedPage() repository.QueryResult {
	result := repository.QueryResult{}
	pages := []domain.Page{}
	rows, err := repo.DB.Query(`SELECT * FROM PAGES WHERE STATUS = 'Published'`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			page := domain.Page{}
			rows.Scan(
				&page.Slug,
				&page.Name,
				&page.Body,
				&page.CreatedAt,
				&page.Status,
			)

			pages = append(pages, page)
		}

		result.Result = pages
	}

	return result
}
func (repo *PageRepositorySQLite) GetAllPages() repository.QueryResult {
	result := repository.QueryResult{}
	pages := []domain.Page{}
	rows, err := repo.DB.Query(`SELECT * FROM PAGES`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			page := domain.Page{}
			rows.Scan(
				&page.Slug,
				&page.Name,
				&page.Body,
				&page.CreatedAt,
				&page.Status,
			)

			pages = append(pages, page)
		}

		result.Result = pages
	}

	return result
}
