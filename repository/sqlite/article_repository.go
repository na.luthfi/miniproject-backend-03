package sqlite

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
)

type ArticleRepositorySQLite struct {
	DB *sql.DB
}

func NewArticleRepositorySQLite(DB *sql.DB) repository.ArticleRepository {
	return &ArticleRepositorySQLite{DB: DB}
}

func (repo *ArticleRepositorySQLite) IsSlugExist(slug string) bool {
	result := repository.QueryResult{}
	article := domain.Article{}

	row := repo.DB.QueryRow(`SELECT * FROM ARTICLES WHERE SLUG = ?`, slug)

	row.Scan(
		&article.Slug,
	)

	result.Result = article

	fmt.Printf("asli: %s | db: %s", slug, article.Slug)
	if article.Slug == slug {
		return true
	}

	return false
}

func (repo *ArticleRepositorySQLite) Save(article *domain.Article) error {
	statement, err := repo.DB.Prepare(`INSERT INTO ARTICLES(SLUG, NAME, BODY, CREATEDAT, STATUS) VALUES(?,?,?,?,?)`)
	if err != nil {
		return err
	}

	statement.Exec(article.Slug, article.Name, article.Body, article.CreatedAt, article.Status)

	statement, err = repo.DB.Prepare(`INSERT INTO ARTICLE_CATEGORIES(SLUG, CATEGORY) VALUES(?,?)`)

	for _, v := range article.Category {
		statement.Exec(article.Slug, v)
	}

	return nil
}

func (repo *ArticleRepositorySQLite) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}
	article := domain.Article{}

	row := repo.DB.QueryRow(`SELECT * FROM ARTICLES WHERE SLUG = ?`, slug)

	row.Scan(
		&article.Slug,
		&article.Name,
		&article.Body,
		&article.CreatedAt,
		&article.Status,
	)

	result.Result = article

	return result
}

func (repo *ArticleRepositorySQLite) ListPublishedArticle() repository.QueryResult {
	result := repository.QueryResult{}
	articles := []domain.Article{}
	rows, err := repo.DB.Query(`SELECT * FROM ARTICLES WHERE STATUS = 'Published'`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			article := domain.Article{}
			rows.Scan(
				&article.Slug,
				&article.Name,
				&article.Body,
				&article.CreatedAt,
				&article.Status,
			)

			// fmt.Println("rows: ", rows)
			// fmt.Println("article: ", article)
			articles = append(articles, article)
		}

		result.Result = articles
	}

	return result
}

func (repo *ArticleRepositorySQLite) ListPublishedArticleByCategory(category string) repository.QueryResult {

	result := repository.QueryResult{}
	articles := []domain.Article{}
	rows, err := repo.DB.Query(`SELECT * FROM ARTICLES INNER JOIN ARTICLE_CATEGORIES ON ARTICLES.SLUG = ARTICLE_CATEGORIES.SLUG WHERE ARTICLE_CATEGORIES.CATEGORY = ?`, category)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			article := domain.Article{}
			rows.Scan(
				&article.Slug,
				&article.Name,
				&article.Body,
				&article.CreatedAt,
				&article.Status,
				&article.Slug,
				&article.Category,
			)
			// fmt.Println("rows: ", rows)
			// fmt.Println("article: ", article)

			articles = append(articles, article)
		}

		result.Result = articles
	}
	// fmt.Println("result.Result: ", articles)

	return result
}
