package sqlite

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
)

func TestCanFindArticleBySlug(t *testing.T) {

	columns := []string{"SLUG", "NAME", "BODY", "CREATED_AT", "STATUS"}

	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM ARTICLES WHERE SLUG = ?`).
		WithArgs("foobar").
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				"foobar",
				"Foobar",
				"Body foobar",
				time.Now(),
				domain.ArticlePublished,
			),
		)

	repo := NewArticleRepositorySQLite(db)

	result := repo.FindBySlug("foobar")

	assert.Nil(t, result.Error)
}

func TestCanFindArticleByCategory(t *testing.T) {
	columns := []string{"SLUG", "NAME", "BODY", "CREATED_AT", "STATUS", "CATEGORY"}

	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM ARTICLES INNER JOIN ARTICLE_CATEGORIES ON ARTICLES.SLUG = ARTICLE_CATEGORIES.SLUG WHERE ARTICLE_CATEGORIES.CATEGORY = ?`).
		WithArgs("News").
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				"foobar",
				"Foobar",
				"Body foobar",
				time.Now(),
				domain.ArticlePublished,
				domain.News,
			),
		)

	repo := NewArticleRepositorySQLite(db)

	result := repo.ListPublishedArticleByCategory("News")

	assert.Nil(t, result.Error)
}
