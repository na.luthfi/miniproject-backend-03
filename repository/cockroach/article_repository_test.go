package cockroach

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
)

func TestCanFindArticleBySlug(t *testing.T) {

	columns := []string{"SLUG", "NAME", "BODY", "CREATED_AT", "STATUS"}

	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM "ARTICLES" WHERE "SLUG" = $1`).
		WithArgs("foobar").
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				"foobar",
				"Foobar",
				"Body foobar",
				time.Now(),
				domain.ArticlePublished,
			),
		)

	repo := NewArticleRepositoryCockroach(db)

	result := repo.FindBySlug("foobar")

	assert.Nil(t, result.Error)
}

// func TestCanFindPublishedArticle(t *testing.T) {

// 	columns := []string{"SLUG", "NAME", "BODY", "CREATED_AT", "STATUS"}

// 	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
// 	mock.ExpectQuery(`SELECT * FROM "ARTICLES" WHERE "STATUS" = "PUBLISHED"`).
// 		WillReturnRows(
// 			sqlmock.NewRows(columns).AddRow(
// 				"foobar",
// 				"Foobar",
// 				"Body foobar",
// 				time.Now(),
// 				domain.ArticlePublished,
// 			),
// 		)

// 	repo := NewArticleRepositoryCockroach(db)

// 	result := repo.ListPublishedArticle()

// 	assert.Nil(t, result.Error)
// }

