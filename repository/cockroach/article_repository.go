package cockroach

import (
	"database/sql"
	"time"

	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
)

// ArticleRepositoryCockroach is imlement article query in cokroach
type ArticleRepositoryCockroach struct {
	DB *sql.DB
}

// ArticleResult is to wrap article result
type ArticleResult struct {
	ID        string
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Status    domain.Status
	Category  []domain.Category
}

// NewArticleRepositoryCockroach is to create ArticleQueryCokroach instance
func NewArticleRepositoryCockroach(DB *sql.DB) repository.ArticleRepository {
	return &ArticleRepositoryCockroach{DB: DB}
}

//Save is to create article to cockroachDB
func (repo *ArticleRepositoryCockroach) Save(article *domain.Article) error {
	var result error

	_, err := repo.DB.Exec(`
		INSERT INTO "ARTICLES" ("SLUG", "NAME", "BODY", "CREATEDAT", "STATUS") 
		VALUES($1,$2,$3,$4,$5)`,
		article.Slug,
		article.Name,
		article.Body,
		article.CreatedAt,
		article.Status,
	)

	if err != nil {
		result = err
	} else {
		result = nil
	}

	return result
}

// FindBySlug is to find published article by slug
func (repo *ArticleRepositoryCockroach) FindBySlug(slug string) repository.QueryResult {
	var result repository.QueryResult

	articleResult := ArticleResult{}
	article := domain.Article{}

	err := repo.DB.QueryRow(`SELECT * FROM "ARTICLES" WHERE "SLUG" = $1`, slug).Scan(
		&articleResult.Slug,
		&articleResult.Name,
		&articleResult.Body,
		&articleResult.CreatedAt,
		&articleResult.Status,
	)

	// If error is found
	if err != nil && err != sql.ErrNoRows {
		result = repository.QueryResult{Error: err}
	}

	// If no rows
	if err == sql.ErrNoRows {
		result = repository.QueryResult{Result: article}
	}

	result = repository.QueryResult{
		Result: domain.Article{
			Slug:      articleResult.Slug,
			Name:      articleResult.Name,
			Body:      articleResult.Body,
			CreatedAt: articleResult.CreatedAt,
			Status:    articleResult.Status,
		},
	}

	return result
}

// IsSlugExist is to find published article by slug
func (repo *ArticleRepositoryCockroach) IsSlugExist(slug string) bool {
	var result repository.QueryResult

	articleResult := ArticleResult{}
	article := domain.Article{}

	err := repo.DB.QueryRow(`SELECT * FROM "ARTICLES" WHERE "SLUG" = $1`, slug).Scan(
		&articleResult.Slug,
		&articleResult.Name,
		&articleResult.Body,
		&articleResult.CreatedAt,
		&articleResult.Status,
	)

	// If error is found
	if err != nil && err != sql.ErrNoRows {
		result = repository.QueryResult{Error: err}
	}

	// If no rows
	if err == sql.ErrNoRows {
		result = repository.QueryResult{Result: article}
	}

	result = repository.QueryResult{
		Result: domain.Article{
			Slug:      articleResult.Slug,
			Name:      articleResult.Name,
			Body:      articleResult.Body,
			CreatedAt: articleResult.CreatedAt,
			Status:    articleResult.Status,
		},
	}

	result.Result = article

	if article.Slug == slug {
		return true
	}

	return false
}

// ListPublishedArticle is to find published article by slug
func (repo *ArticleRepositoryCockroach) ListPublishedArticle() repository.QueryResult {
	var result repository.QueryResult
	articles := []domain.Article{}

	rows, err := repo.DB.Query(`SELECT * FROM "ARTICLES" WHERE "STATUS" = "Published"`)
	//rows, err := repo.DB.Query(`SELECT * FROM "ARTICLES"`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			article := domain.Article{}
			rows.Scan(
				&article.Slug,
				&article.Name,
				&article.Body,
				&article.CreatedAt,
				&article.Status,
			)

			articles = append(articles, article)
		}

		result.Result = articles
	}

	// result = repository.QueryResult{
	// 	Result: domain.Article{
	// 		Slug:      articleResult.Slug,
	// 		Name:      articleResult.Name,
	// 		Body:      articleResult.Body,
	// 		CreatedAt: articleResult.CreatedAt,
	// 		Status:    articleResult.Status,
	// 	},
	// }

	return result
}

// ListPublishedArticleByCategory is to find published article by slug
func (repo *ArticleRepositoryCockroach) ListPublishedArticleByCategory(category string) repository.QueryResult {
	var result repository.QueryResult

	articleResult := ArticleResult{}
	articles := []domain.Article{}

	rows, err := repo.DB.Query(`SELECT * FROM "ARTICLES" INNER JOIN "ARTICLE_CATEGORIES" ON "ARTICLES.SLUG" = "ARTICLE_CATEGORIES.SLUG" WHERE "CATEGORY" = $1`, category)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			article := domain.Article{}
			rows.Scan(
				&article.Slug,
				&article.Name,
				&article.Body,
				&article.CreatedAt,
				&article.Status,
			)

			articles = append(articles, article)
		}

		result.Result = articles
	}

	result = repository.QueryResult{
		Result: domain.Article{
			Slug:      articleResult.Slug,
			Name:      articleResult.Name,
			Body:      articleResult.Body,
			CreatedAt: articleResult.CreatedAt,
			Status:    articleResult.Status,
		},
	}

	return result
}
