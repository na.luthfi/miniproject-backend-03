package repository

import "gitlab.com/na.luthfi/miniproject-backend-03/domain"

// QueryResult is wrapper query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleRepository is wrap article repository
type ArticleRepository interface {
	Save(article *domain.Article) error
	IsSlugExist(slug string) bool
	FindBySlug(slug string) QueryResult
	ListPublishedArticle() QueryResult
	ListPublishedArticleByCategory(category string) QueryResult
}

// PageRepository is wrap page repository
type PageRepository interface {
	SavePage(page *domain.Page) error
	IsPageLimited() bool
	GetAllPages() QueryResult
	IsSlugExist(slug string) bool
}
