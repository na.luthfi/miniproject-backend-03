package config

import (
	"database/sql"
	"io/ioutil"

	"gitlab.com/na.luthfi/miniproject-backend-03/repository/cockroach"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository/inmemory"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository/sqlite"
	"gitlab.com/na.luthfi/miniproject-backend-03/storage"
)

func InitArticleInMemory() repository.ArticleRepository {
	storage := storage.CreateArticleStorage()

	return inmemory.NewArticleRepositoryInMemory(storage)
}

func InitArticleSQLite() repository.ArticleRepository {
	path := "database/sqlite/app.sqlite"
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		panic(err)
	}

	//Run DDL
	ddl, err := ioutil.ReadFile("database/sqlite/ddl.sql")
	if err != nil {
		panic(err)
	}

	sql := string(ddl)

	_, err = db.Exec(sql)
	if err != nil {
		panic(err)
	}
	return sqlite.NewArticleRepositorySQLite(db)
}

func InitPageInMemory() repository.PageRepository {
	storage := storage.CreatePageStorage()

	return inmemory.NewPageRepositoryInMemory(storage)
}

func InitPageSQLite() repository.PageRepository {
	path := "database/sqlite/app.sqlite"
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		panic(err)
	}

	//Run DDL
	ddl, err := ioutil.ReadFile("database/sqlite/ddl.sql")
	if err != nil {
		panic(err)
	}

	sql := string(ddl)

	_, err = db.Exec(sql)
	if err != nil {
		panic(err)
	}
	return sqlite.NewPageRepositorySQLite(db)
}

// InitCockroach is init cockroachdb connection
func InitCockroach() repository.ArticleRepository {
	dsn := "postgresql://root@localhost:26257/kumparan?sslmode=disable"
	db, err := sql.Open("postgres", dsn)

	if err != nil {
		panic(err)
	}

	// defer db.Close()
	// db.Exec(`set search_path='public'`)

	return cockroach.NewArticleRepositoryCockroach(db)
}

// // InitInMemory is to init in memory database connection
// func InitInMemory() *storage.ArticleStorage {
// 	return storage.CreateArticleStorage()
// }
