package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"github.com/urfave/cli"
	"gitlab.com/na.luthfi/miniproject-backend-03/config"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain"
	"gitlab.com/na.luthfi/miniproject-backend-03/domain/services"
	"gitlab.com/na.luthfi/miniproject-backend-03/repository"
)

var driver = "sqlite"

func menuArticle() {
	var menu int

	fmt.Println("Daftar Menu:")
	fmt.Printf("1. Cari Artikel Berdasarkan Slug\n2. Cari Artikel Berdasarkan Kategori\n3. Tambahkan Artikel\n4. Tampilkan Artikel yang Dipublikasi\n5. Tampilkan Page\n6. Keluar\n")
	fmt.Printf("Pilih menu yang ingin dijalankan: ")
	fmt.Scanf("%d", &menu)
	switch menu {
	case 1:
		FindArticleBySlug()
	case 2:
		FindArticleByCategory()
	case 3:
		SaveArticle()
	case 4:
		ListPublishedArticle()
	case 5:
		ListAllPage()
	case 6:
		os.Exit(3)
	default:
		{
			fmt.Println("Masukan anda salah")
			menuArticle()
		}
	}
}

func main() {
	fmt.Println("***********************************************************************************************************")
	ListPublishedArticle()
	fmt.Println("***********************************************************************************************************")
	fmt.Println("***********************************************************************************************************")
	ListAllPage()
	fmt.Println("***********************************************************************************************************")
	menuArticle()
	// startCLI()
}

func startCLI() {
	app := cli.NewApp()
	app.Name = "terminal kumparan"
	app.Usage = "Menyimpan dan menampilkan artikel dan page"
	app.Version = "0.0.1"

	articleFlags := []cli.Flag{
		cli.StringFlag{
			Name:  "name,n",
			Value: "",
			Usage: "title of your article",
		},
		cli.StringFlag{
			Name:  "slug,s",
			Value: "",
			Usage: "unique slug of your article",
		},
		cli.StringFlag{
			Name:  "body,b",
			Value: "",
			Usage: "body of your article",
		},
		cli.StringFlag{
			Name:  "status,b",
			Value: "",
			Usage: "status of your article (Published or NotPublished)",
		},
		cli.StringFlag{
			Name:  "category,c",
			Value: "",
			Usage: "insert category (max. 4)",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:    "createArticle",
			Aliases: []string{"catc"},
			Usage:   "Create article",
			Flags:   articleFlags,
			Action:  cArticle,
		},
		// {
		// 	Name:    "createPage",
		// 	Aliases: []string{"cpg"},
		// 	Usage:   "Create page, max page is 3",
		// 	Flags:   articleFlags,
		// 	Action:  cArticle,
		// },
		// {
		// 	Name:    "getPublishArticle",
		// 	Aliases: []string{"gpat"},
		// 	Usage:   "get published articles",
		// 	Action:  pArticlePublished,
		// },
		// {
		// 	Name:    "getPublishArticleByCategory",
		// 	Aliases: []string{"gpac"},
		// 	Usage:   "get published articles by category, require article category",
		// 	Flags: []cli.Flag{cli.StringFlag{
		// 		Name:  "category,c",
		// 		Value: "",
		// 		Usage: "category to help find article",
		// 	},
		// 	},
		// 	Action: pArticleByCategory,
		// },
		// {
		// 	Name:    "showArticleAndPage",
		// 	Aliases: []string{"i"},
		// 	Usage:   "Show All Agenda",
		// 	Action: func(c *cli.Context) error {

		// 		return nil
		// 	},
		// },
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func cArticle(c *cli.Context) error {
	name := c.String("name")
	slug := c.String("slug")
	body := c.String("body")
	status := c.String("status")

	category := c.String("category")
	var categories []domain.Category
	categories = append(categories, domain.Category(category))
	repo := config.InitArticleSQLite()
	article := domain.Article{
		Name:     name,
		Slug:     slug,
		Body:     body,
		Status:   domain.Status(status),
		Category: categories,
	}
	err := repo.Save(&article)
	if err != nil {
		return err
	}
	return nil
}

func SaveArticle() {
	var slug string
	var status domain.Status
	var category []domain.Category
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Masukkan slug: ")
	fmt.Scanf("%s", &slug)
	fmt.Printf("Masukkan judul: ")
	name, _ := reader.ReadString('\n')
	fmt.Printf("Masukkan isi: ")
	body, _ := reader.ReadString('\n')
	fmt.Printf("Masukkan status artikel(Published atau NotPublished): ")
	fmt.Scanf("%s", &status)
	categories := InputCategory(category)

	var repo repository.ArticleRepository
	switch driver {
	case "sqlite":
		repo = config.InitArticleSQLite()
	case "cockroach":
		repo = config.InitCockroach()
	default:
		repo = config.InitArticleInMemory()
	}

	articleService := services.NewArticleServiceInMemory(repo)
	article, err := domain.CreateArticle(articleService, slug, name, body, status, categories...)
	if err != nil {
		fmt.Println(err)
	} else {
		repo.Save(&article)
		fmt.Println("Menyimpan artikel", article.Name)
		menuArticle()
	}
}

func PrintArticle(article domain.Article) {
	fmt.Printf("Slug: %s\n", article.Slug)
	fmt.Printf("Name: %s", article.Name)
	fmt.Printf("Body: %s\n", article.Body)

	for k, v := range article.Category {
		fmt.Printf("Kategori ke-%d: %s", k, v)
	}
}

func InputCategory(category []domain.Category) []domain.Category {
	var numberOfCategory int
	var tempCategory domain.Category

	fmt.Printf("Jumlah kategori yang ingin anda masukkan (Max. 4): ")
	fmt.Scanf("%d", &numberOfCategory)
	if numberOfCategory > 4 {
		fmt.Println("Maaf masukan anda salah")
		category = InputCategory(category)
	} else {
		for i := 0; i < numberOfCategory; i++ {
			fmt.Printf("Masukkan kategori ke-%d (News, Politic, Entertainment, Internet): ", i+1)
			fmt.Scanf("%s", &tempCategory)
			category = append(category, tempCategory)
		}
	}

	return category
}

func FindArticleBySlug() {
	var slug string

	fmt.Printf("Masukkan slug: ")
	fmt.Scanf("%s", &slug)

	var repo repository.ArticleRepository
	switch driver {
	case "sqlite":
		repo = config.InitArticleSQLite()
	case "cockroach":
		repo = config.InitCockroach()
	default:
		repo = config.InitArticleInMemory()
	}

	result := repo.FindBySlug(slug)
	if result.Error != nil {
		fmt.Printf("Artikel dengan slug %s tidak ditemukan", slug)
		fmt.Println(result.Error)
	} else {
		fmt.Println("=================================================")
		PrintArticle(result.Result.(domain.Article))
		fmt.Println("=================================================")
	}

	//fmt.Println("Menampilkan berita berdasarkan slug", result.Result)
	menuArticle()
}

func FindArticleByCategory() {
	var category string

	fmt.Printf("Masukkan kategori: ")
	fmt.Scanf("%s", &category)

	var repo repository.ArticleRepository
	switch driver {
	case "sqlite":
		repo = config.InitArticleSQLite()
	case "cockroach":
		repo = config.InitCockroach()
	default:
		repo = config.InitArticleInMemory()
	}

	result := repo.ListPublishedArticleByCategory(category)

	for k, v := range result.Result.([]domain.Article) {
		fmt.Println(k + 1)
		PrintArticle(v)
		fmt.Println("=================================================")
	}
	// fmt.Println("Menampilkan berita berdasarkan kategori", result.Result)
	menuArticle()
}

func ListPublishedArticle() {
	var repo repository.ArticleRepository
	switch driver {
	case "sqlite":
		repo = config.InitArticleSQLite()
	case "cockroach":
		repo = config.InitCockroach()
	default:
		repo = config.InitArticleInMemory()
	}
	result := repo.ListPublishedArticle()
	fmt.Println("Daftar Artikel:")

	for k, v := range result.Result.([]domain.Article) {
		fmt.Println(k + 1)
		PrintArticle(v)
		fmt.Println("=================================================")
	}
	//fmt.Println(result.Result)
	menuArticle()
}

func PrintPage(page domain.Page) {
	fmt.Printf("Slug: %s\n", page.Slug)
	fmt.Printf("Name: %s\n", page.Name)
	fmt.Printf("Body: %s\n", page.Body)
}

func ListAllPage() {
	var repo repository.PageRepository
	switch driver {
	case "sqlite":
		repo = config.InitPageSQLite()
	default:
		repo = config.InitPageInMemory()
	}
	result := repo.GetAllPages()
	fmt.Println("Daftar Page:")

	for k, v := range result.Result.([]domain.Page) {
		fmt.Println(k + 1)
		PrintPage(v)
		fmt.Println("=================================================")
	}
	menuArticle()
}
